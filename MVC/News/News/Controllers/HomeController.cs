﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using News.Models;
using News.Services;

namespace News.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new NewsModel
            {
                News = GetNews()
            };
            return View(model);
        }

        public ActionResult IndexWithJavascript()
        {
            var model = new NewsModel
            {
                News = GetNews()
            };
            return View(model);
        }

        public ActionResult SearchNews(string pattern)
        {
            pattern = pattern?.ToLower() ?? string.Empty;
            var model = new NewsModel
            {
                News = GetNews()
                    .Where(n => n.Title.ToLower().Contains(pattern)
                                || n.Text.ToLower().Contains(pattern))
                    .ToList()
            };

            return PartialView("_PartialNews", model);
        }

        private List<NewsEntry> GetNews()
        {
            return NewsStorage.News;
        }
    }
}