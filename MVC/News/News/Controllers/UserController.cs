﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using News.Models;
using News.Services;

namespace News.Controllers
{
    public class UserController : Controller
    {
        private static readonly object userlock = new object();

        public ActionResult Index()
        {
            var models = GetLighweightUsers();

            return View(models);
        }

        [HttpGet]
        public ActionResult UserDetails(int id)
        {
            var user = UserStorage.Users.FirstOrDefault(s => s.Id == id);
            if (user == null)
                return View("NotFound");

            return View(user);
        }

        [HttpPost]
        public ActionResult UserDetails(UserDetailsModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            UserStorage.Users.RemoveAll(s => s.Id == model.Id);
            UserStorage.Users.Add(model);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View(new UserDetailsModel());
        }

        [HttpPost]
        public ActionResult Register(UserDetailsModel model)
        {

            if (!ModelState.IsValid)
                return View(model);

            lock (userlock)
            {
                model.Id = UserStorage.Users.DefaultIfEmpty(new UserDetailsModel()).Max(s => s.Id) + 1;
                UserStorage.Users.Add(model);
            }

            return RedirectToAction("Index");
        }


        private static List<UserModel> GetLighweightUsers()
        {
            return UserStorage.Users.Select(s => new UserModel
            {
                FirstName = s.FirstName,
                LastName = s.LastName,
                Id = s.Id
            }).ToList();
        }
    }
}