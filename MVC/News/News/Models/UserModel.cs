﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace News.Models
{
    public class UserModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "First Name", Order = 1)]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Last Name", Order = 2)]
        public string LastName { get; set; }
    }
}