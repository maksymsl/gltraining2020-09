﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using News.Models;

namespace News.Services
{
    public static class UserStorage
    {
        public static List<UserDetailsModel> Users { get; } = new List<UserDetailsModel>
        {
            new UserDetailsModel { BirthDate = new DateTime(1924, 1, 1), Email = "asdas1@example.com", Id = 1, FirstName = "AAAAA", Password = "1234"},
            new UserDetailsModel { BirthDate = new DateTime(1924, 1, 1), Email = "asdas2@example.com", Id = 2, FirstName = "ZZZZZ", Password = "1234"}
        };
    }
}