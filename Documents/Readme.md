# Recordings

* C# 2: https://globallogic.zoom.us/rec/share/9XHJx8-qXTJUG12DwxQKFBg2l6OM39RkcIM0ghaCEyhN9ulBBWTJ0jE_XGCWZC4.bZF53jChs31v6YVd
* C# 3: https://globallogic.zoom.us/rec/share/boLLWoEPq0tcJYqTkwkuu1lEvDRaAmwatswME8a27w59c5ZekHs2wVALm8idgSsB.EfC7JBtsZf5ga2Ss
* WCF 1: https://globallogic.zoom.us/rec/share/XzAmYCOVpfsLJucA06ktm4_u1GuVy634-88no4MfH_WrBIV6RQxBYYheVZsAadI9.-goy6aEIsPRdU0CC
* WCF 2: https://globallogic.zoom.us/rec/share/d9frZ-t6Ph21JKxPYTcrqWMedU3hFJDdcrQ5JvPWUHK7z-h74GowcXqLhlD8qh5L.Klq5RDrcA5yJugC5
* MVC 1: https://globallogic.zoom.us/rec/share/tBuYRo-N9k7JyHDY09waEcWQr9CKIFBEtTqMurRgemBfIR0HQ6w79kYj_dvHtLtu.xO48v3k0QmWMqgix
* MVC 2: https://globallogic.zoom.us/rec/share/dFFwcHkaDt9GsauEbPnXvhgIkmBVlqdQ9_dkMRwnFiycbfTPs6_8QFAErBdFCyHs.Ja8wzcPNC9xp8LpG
* MVC 3 (+ Core): https://globallogic.zoom.us/rec/share/gzY4hrlUP3O5MGa78l5PVfL04Dl9emk150Z9nwZl1pb9aCI4mILpe5-pzlLD-gkW.4UDPunQjm81lBODz