﻿using System;
using System.Runtime.Serialization;

namespace GLTraining.WCF.ForumAPI.Models
{
    [DataContract]
    public class Post
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public DateTime DtCreated { get; set; }
        [DataMember]
        public string Author { get; set; }

        [IgnoreDataMember]
        public string WhatAmIDoingHere { get; set; }
    }
}
