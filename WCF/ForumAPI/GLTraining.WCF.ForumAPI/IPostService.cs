﻿using System.Collections.Generic;
using System.ServiceModel;
using GLTraining.WCF.ForumAPI.Models;

namespace GLTraining.WCF.ForumAPI
{
    [ServiceContract]
    public interface IPostService
    {
        [OperationContract]
        List<Post> GetPosts();

        [OperationContract(Name = "GetFilteredPosts")]
        List<Post> GetPosts(DateRange range);

        [OperationContract]
        void AddPost(Post post);
    }
}
