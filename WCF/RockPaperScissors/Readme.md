# Rock-Paper-Scissors

## Console client
Implement a console application which can be used as a rock-paper-scissors matchmaker. Player names are hard-coded and equal to: Alice, Bob. Use case scenario:
1. User launches a new match from console
2. Application queries WCF service for player shapes and selects a winner
3. Application shows winner in console and sends match info to WCF service
4. When user presses “Q”, application displays statistics in console in format

    "`player name`: number of matches won” and exits.


## WCF Service
Implement a WCF service with the following interface:
1. GetShape() - returns random shape between rock, paper, scissors
2. void AddMatchInfo(MatchInfo m) - stores given match info. Match info consists of:
    
    `DtPlayed` - date and time when match was played
    
    `WinnerName` - name of the winner
3. GetHistory() - returns info about all recorded matches (provided via AddMatchInfo method) 
