Calculator

Implement console application that works as a calculator (supports multiple arithmetic operations):
* 	Application should read statements from console and evaluate each of them. 
* 	Supported operators: `+, -, *, /`. 
* 	Supported operand types: integer. 
* 	Supported number of operands: 2.
* 	Example input: `42 + 123`, output: `165`.

Exceptions should be displayed in console window and should not crash the application.

Optional requirements:	
* support any number of operands. E.g., `2 + 3 - 5`. Bracket support is optional (up to you).
* recover from errors, if possible. E.g., if input for is corrupted (`2 + `), ask user to re-enter data.
