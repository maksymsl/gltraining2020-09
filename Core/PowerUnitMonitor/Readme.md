# V1

Implement a class PowerUnit, which stores 
* id (`int`), 
* heat level (`int`) 
* heat level threshold (`int`). 

PowerUnit should trigger an event whenever heat level is set equal to or above the heat threshold, passing id and current heat level as event args.

Implement a console application, which does the following:
* prompts user for number of power units and heat threshold. Threshold may be the same for all power units or entered separately for each - up to you
* creates power units with `0` heat
* each time user clicks "Enter", system increases heat level of each power unit for a random value

Whenever overheat event is triggered, system should notify user about it (write info to console) and reset overheated power unit (set heat level back to `0`).


# V2

1. Replace manual heat update with automatic update in background thread. 

   Instead of updating heat level after “Enter” click, background thread should do it every `2 sec`.
2. Refactor PowerUnit so that it supports the following subtypes.
   Utilize inheritance and subtype polymorphism
    1. `RandomPowerUnit` - already implemented. On each tick heat is increased by a random value
    2. `LinearPowerUnit` - on each tick heat is increased by a constant value
    3. `QuadraticPowerUnit` - on each tick heat is increased by formula: `Heat = Heat + TickNumber * TickNumber`

       Assuming initial heat is 0:

       1st tick: Heat = 1

       2nd tick: Heat = 1 + 2*2 = 5

       3rd tick: Heat = 5 + 3*3 = 14
3. Cover PowerUnit-s with unit tests. Use whatever test framework you prefer.
